package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {

    static  final String APIBaseURL="/apitechu/v1";

    @RequestMapping(APIBaseURL+"/products")
    public ArrayList<ProductModel> getProducts(){

        System.out.println("getProducts");

        return ApitechuApplication.productModels;

    }

    @RequestMapping(APIBaseURL+"/products/{id}")
    public ProductModel getProductId(@PathVariable String id){

        System.out.println("getProductId");
        System.out.println("id es " + id);

        ProductModel result = new ProductModel();

        for(ProductModel product : ApitechuApplication.productModels){
            if(product.getId().equals(id)){
                result=product;
            }
        }

        return result;

    }

    @PostMapping(APIBaseURL+ "/products")

    public ProductModel createProduct(@RequestBody ProductModel newProduct){
        System.out.println("createProduct");
        System.out.println("La id del producto es "+ newProduct.getId());
        System.out.println("La id del descripción es " + newProduct.getDesc());
        System.out.println("El id del precio es " + newProduct.getPrice());

        ApitechuApplication.productModels.add(newProduct);
        return newProduct;
    }

    @PutMapping(APIBaseURL + "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id){

        System.out.println("updateProduct");
        System.out.println("La id del producto a actualizar en parametrode URL es " + id);
        System.out.println("La id del producto a actualizar  es " +product.getId());
        System.out.println("La descripcion producto a actualizar  es " + product.getDesc());
        System.out.println("El precio del producto a actualizar  es " + product.getPrice());

        for(ProductModel productInList: ApitechuApplication.productModels){
            if(productInList.getId().equals(id)){
                productInList.getId().equals(id);
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());

            }
        }

        return  product;

    }

    @DeleteMapping(APIBaseURL+"/products/{id}")
    public  ProductModel deleteProduct(@PathVariable String id){
        System.out.println("DeleteProduct");
        System.out.println("La id del producto a borrar es " + id);

        ProductModel result = new ProductModel();

        boolean foundCompany= false;

        for(ProductModel productInList : ApitechuApplication.productModels){
            if(productInList.getId().equals(id)){
                System.out.println("Producto encontrado");
                foundCompany = true;
                result= productInList;

            }

            if(foundCompany==true){
                System.out.println("Borrando producto");
                ApitechuApplication.productModels.remove(result);
            }
        }

        return result;

    }

    @PatchMapping(APIBaseURL + "/products/{id}")
    public ProductModel updatePartialProduct(@RequestBody ProductModel product, @PathVariable String id){
        System.out.println("updatePartialProduct");
        System.out.println("La id del producto a actualizar en parametrode URL es " + id);
        System.out.println("La id del producto a actualizar  es " +product.getId());
        System.out.println("La descripcion producto a actualizar  es " + product.getDesc());
        System.out.println("El precio del producto a actualizar  es " + product.getPrice());

        ProductModel result= new ProductModel();

        for(ProductModel productInList: ApitechuApplication.productModels){


            System.out.println("----------------");

            if(productInList.getId().equals(id)) {
                result = productInList;

                if (product.getDesc() != null) {
                    System.out.println("La descirpción modificada es " + product.getDesc());
                    productInList.setDesc(product.getDesc());

                }
                if (product.getPrice() > 0) {
                    System.out.println("El precio modificado es " + product.getPrice());
                    productInList.setPrice(product.getPrice());

                }
            }

        }


        return  result;
    }



}
